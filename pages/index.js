import data from '../assets/factions.json'
import List from '../components/List.js';

const Index = props => (
  <div>
    <ul>
      {props.factions.map(faction => (
        <List key={faction.id}>
          {faction.name}
        </List>
      ))}
    </ul>
    <style jsx>{`
    ul {
      border: 1px solid lightgrey;
      display: flex;
      flex-flow: column nowrap; 
      margin: auto;
      padding: 0.2% 0 0.2% 0;
      width: 50%;
  }
  `}</style>
  </div>
);

Index.getInitialProps = async function () {

  return {
    factions: data.map(entry => entry.faction)
  };
};

export default Index;
