const List = props => (
    <div>
        <li key={props.id}>
          <a>{props.children}</a>
        </li>
    <style jsx>{`
  li {
      list-style: none;
      padding: 1% 0% 1% 1%;
  }
  
  li:hover {
      background-color: rgba(0, 0, 0, 0.05);
      cursor: pointer;
  }
  
  li:active {
      background-color: rgba(0, 0, 0, 0.2);
      cursor: pointer;
  }
  `}</style>
  </div>
);

  
export default List;